import React from 'react';
import styled from 'styled-components';
import colors from '../../utils/style/colors';

const Container = styled.div``;

const Heading = styled.h1`
  font-weight: 500;
  font-size: 48px;
  margin: 0;
  margin-bottom: 40px;

  span {
    color: ${colors.secondary};
  }
`;

const SubHeading = styled.h2`
  font-weight: normal;
  font-size: 18px;
  margin: 0;
`;

/** Renders the heading of profile page */
const ProfileHeading: React.FC<{ userFirstname: string }> = ({ userFirstname }) => {
  return (
    <Container>
      <Heading>
        Bonjour
        <span>{` ${userFirstname}`}</span>
      </Heading>
      <SubHeading>Félicitation ! Vous avez explosé vos objectifs hier 👏</SubHeading>
    </Container>
  );
};

export default ProfileHeading;
