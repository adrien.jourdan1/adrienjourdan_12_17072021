import React from 'react';
import styled from 'styled-components';
import colors from '../../../../utils/style/colors';

const Container = styled.div`
  width: 258px;
  padding: 32px;
  background-color: ${colors.background};
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 25px;
`;

const Icon = styled.div`
  height: 60px;
  width: 60px;
  display: flex;
  border-radius: 5px;
  background-color: red;

  img {
    height: 20px;
    margin: auto;
  }
`;

const Description = styled.div`
  p {
    margin: 0;
    font-weight: 500;
    font-size: 14px;
    color: #74798c;
  }

  h3 {
    margin: 0;
    margin-bottom: 5px;
    font-weight: 600;
    font-size: 20px;
  }
`;

interface IProps {
  value: number;
  unit: 'Calories' | 'Proteines' | 'Glucides' | 'Lipides';
}

/**
 * Renders user's key figure as a card
 * @param   {number}   value    key figure value
 * @param   {string}   unit     key figure unit (ex: 'Calories')
 */
const KeyFigureCard: React.FC<IProps> = (props) => {
  const { value, unit } = props;

  enum EIconUrl {
    'Proteines' = '/assets/proteins-icon.svg',
    'Glucides' = '/assets/carbs-icon.svg',
    'Lipides' = '/assets/lipids-icon.svg',
    'Calories' = '/assets/calories-icon.svg',
  }

  enum EIconBgColor {
    'Proteines' = 'rgba(96,178,228,0.1)',
    'Glucides' = 'rgba(249,206,36,0.1)',
    'Lipides' = 'rgba(253,81,129,0.1)',
    'Calories' = 'rgba(255,0,0,0.1)',
  }

  enum EUnit {
    'Proteines' = 'g',
    'Glucides' = 'g',
    'Lipides' = 'g',
    'Calories' = 'kCal',
  }

  return (
    <Container>
      <Icon style={{ backgroundColor: EIconBgColor[unit] }}>
        <img src={EIconUrl[unit]} alt={`icon ${unit}`} />
      </Icon>
      <Description>
        <h3>{value + EUnit[unit]}</h3>
        <p>{unit}</p>
      </Description>
    </Container>
  );
};

export default KeyFigureCard;
