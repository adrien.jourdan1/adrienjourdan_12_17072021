import React from 'react';
import styled from 'styled-components';
import { IUserDataInfo } from '../../../../utils/interfaces/api.interface';
import KeyNumberCard from './KeyFigureCard';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  max-height: 615px;

  @media (max-width: 1350px) {
    flex-flow: row wrap;
    max-width: 855px;
  }
`;

enum EKeyData {
  'calorieCount' = 'Calories',
  'proteinCount' = 'Proteines',
  'carbohydrateCount' = 'Glucides',
  'lipidCount' = 'Lipides',
}

/** Renders the user's key figures container */
const ProfileBodyKeys: React.FC<{ data: IUserDataInfo }> = ({ data }) => {
  return (
    <Container>
      {Object.entries(data.keyData).map(([key, value]) => (
        <KeyNumberCard
          value={value}
          unit={EKeyData[key as keyof typeof EKeyData] as 'Calories' | 'Proteines' | 'Glucides' | 'Lipides'}
          key={EKeyData[key as keyof typeof EKeyData]}
        />
      ))}
    </Container>
  );
};

export default ProfileBodyKeys;
