import React from 'react';
import styled from 'styled-components';
import { ResponsiveContainer, RadialBar, RadialBarChart, PolarAngleAxis } from 'recharts';
import colors from '../../../../utils/style/colors';

const Container = styled.div`
  height: 265px;
  background-color: ${colors.background};
  border-radius: 5px;
  position: relative;
  width: 265px;
`;

const ChartTitle = styled.h3`
  margin: 0;
  font-size: 15px;
  font-weight: 500;
  position: absolute;
  top: 25px;
  left: 30px;
`;

const GoalResume = styled.div`
  position: absolute;
  height: 100px;
  width: 100px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  top: calc(50% - 50px);
  right: calc(50% - 50px);

  strong {
    margin: 0;
    font-size: 26px;
    font-weight: 700;
    text-align: center;
  }

  span {
    margin: 0;
    font-size: 16px;
    font-weight: 500;
    text-align: center;
    line-height: 26px;
    color: #6e7c83;
  }
`;

/**
 * Renders the performance graphic
 * @param   {number}   score   user's score
 */
const Score: React.FC<{ score: number }> = ({ score }) => {
  const data = [{ uv: score * 100, fill: colors.secondary }];

  return (
    <Container>
      <ResponsiveContainer width="100%" height="100%">
        <RadialBarChart
          width={200}
          height={200}
          data={data}
          barSize={10}
          startAngle={70}
          endAngle={430}
          innerRadius="62.5%"
        >
          <PolarAngleAxis type="number" domain={[0, 100]} dataKey="uv" angleAxisId={0} tick={false} />
          <RadialBar dataKey="uv" angleAxisId={0} data={data} />
        </RadialBarChart>
      </ResponsiveContainer>
      <GoalResume>
        <strong>{score * 100}%</strong>
        <span> de votre objectif</span>
      </GoalResume>
      <ChartTitle>Score</ChartTitle>
    </Container>
  );
};

export default Score;
