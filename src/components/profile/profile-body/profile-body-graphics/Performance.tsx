import React from 'react';
import styled from 'styled-components';
import { ResponsiveContainer, PolarAngleAxis, PolarGrid, Radar, RadarChart } from 'recharts';
import colors from '../../../../utils/style/colors';
import { IUserDataPerformance, IUserDataPerformanceKind } from '../../../../utils/interfaces/api.interface';

const Container = styled.div`
  height: 265px;
  background-color: ${colors.grey};
  border-radius: 5px;
  width: 265px;

  .recharts-polar-grid-angle {
    display: none;
  }
`;

/**
 * Renders the performance graphic
 * @param   {IUserDataPerformance}   performanceData   contains user's performance data
 */
const Performance: React.FC<{ performanceData: IUserDataPerformance }> = ({ performanceData }) => {
  // get the correct axis label from performance.kind
  const getAxisLabels = (data: { value: number; kind: number }) => {
    const { kind } = performanceData;

    enum EPerformanceTrad {
      'intensity' = 'Intensité',
      'speed' = 'Vitesse',
      'strength' = 'Force',
      'endurance' = 'Endurance',
      'energy' = 'Energie',
      'cardio' = 'Cardio',
    }

    return (
      // if no trad enum
      // kind[data.kind as keyof IUserDataPerformanceKind].charAt(0).toUpperCase() +
      // kind[data.kind as keyof IUserDataPerformanceKind].slice(1)
      EPerformanceTrad[kind[data.kind as keyof IUserDataPerformanceKind]]
    );
  };

  // Reverse the Array to correctly look like the mockup
  const reverseArray = (arr: any[]) => arr.reduce((acc, val) => [val, ...acc], []);
  const test = reverseArray(performanceData.data);

  return (
    <Container>
      <ResponsiveContainer width="100%" height="100%">
        <RadarChart data={test} margin={{ top: 20, bottom: 20, right: 20, left: 20 }}>
          <PolarGrid />
          <PolarAngleAxis
            dataKey={getAxisLabels}
            stroke="#fff"
            tickLine={false}
            tick={{ fill: 'white', fontSize: 12, fontWeight: 500 }}
          />
          <Radar dataKey="value" fill={colors.secondary} fillOpacity={0.7} />
        </RadarChart>
      </ResponsiveContainer>
    </Container>
  );
};

export default Performance;
