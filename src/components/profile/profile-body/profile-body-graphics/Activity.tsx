import React from 'react';
import styled from 'styled-components';
import { ResponsiveContainer, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import colors from '../../../../utils/style/colors';
import { IUserDataActivity } from '../../../../utils/interfaces/api.interface';

const Container = styled.div`
  height: 320px;
  background-color: ${colors.background};
  border-radius: 5px;
  position: relative;
  width: 855px;

  .recharts-legend-item-text {
    color: #74798c !important;
    font-size: 14px;
    font-weight: 500;
  }
`;

const ChartTitle = styled.h3`
  margin: 0;
  font-size: 15px;
  font-weight: 500;
  position: absolute;
  top: 25px;
  left: 30px;
`;

const CustomTooltip = styled.div`
  display: flex;
  flex-direction: column;
  gap: 7px;
  background-color: ${colors.secondary};
  padding: 15px 8px;

  p {
    margin: 0;
    text-align: center;
    color: white;
    font-size: 11px;
    font-weight: 500;
  }
`;

/**
 * Renders the acitvity graphic
 * @param   {IUserDataActivity}   activityData   contains user's sessions data
 */
const Activity: React.FC<{ activityData: IUserDataActivity }> = ({ activityData }) => {
  const { sessions } = activityData;

  const dates = sessions.map((elt) => elt.day);

  const kilograms = sessions.map((elt) => elt.kilogram);
  const minKilo = Math.min(...kilograms) - 5;
  const maxKilo = Math.max(...kilograms) + 5;

  const calories = sessions.map((elt) => elt.calories);
  const minCal = Math.min(...calories) - 50;
  const maxCal = Math.max(...calories) + 50;

  const getCustomTooltip = (data: any) => {
    const { payload, active } = data;
    if (active && payload && payload.length) {
      return (
        <CustomTooltip>
          <p>{`${payload[0].payload.kilogram}kg`}</p>
          <p>{`${payload[0].payload.calories}Kcal`}</p>
        </CustomTooltip>
      );
    }

    return null;
  };

  // get the correct axis label from index
  const getAxisLabels = (data: any) => {
    let dayIndex = dates.findIndex((day) => day === data.day);
    dayIndex += 1;
    return dayIndex;
  };

  return (
    <Container>
      <ResponsiveContainer width="100%" height={320}>
        <BarChart
          data={activityData.sessions}
          barSize={7}
          barGap={8}
          margin={{
            top: 25,
            right: 25,
            left: 25,
            bottom: 25,
          }}
        >
          <CartesianGrid strokeDasharray="2 2" vertical={false} />
          <XAxis dataKey={getAxisLabels} tickLine={false} dy={15} />
          <YAxis yAxisId="calories" hide domain={[minCal, maxCal]} />
          <YAxis
            yAxisId="kilo"
            orientation="right"
            allowDecimals={false}
            tickLine={false}
            axisLine={false}
            tick={{ fontSize: 14 }}
            domain={[minKilo, maxKilo]}
          />
          <Tooltip content={getCustomTooltip} />
          <Legend verticalAlign="top" align="right" iconType="circle" iconSize={8} height={70} />
          <Bar yAxisId="kilo" name="Poids (kg)" dataKey="kilogram" unit="kg" fill={colors.primary} />
          <Bar
            yAxisId="calories"
            name="Calories brûlées (kCal)"
            dataKey="calories"
            unit="kCal"
            fill={colors.secondary}
          />
        </BarChart>
      </ResponsiveContainer>
      <ChartTitle>Activité quotidienne</ChartTitle>
    </Container>
  );
};

export default Activity;
