import React from 'react';
import styled from 'styled-components';
import { IUserData } from '../../../../utils/interfaces/api.interface';
import Activity from './Activity';
import AverageSessions from './AverageSessions';
import Performance from './Performance';
import Score from './Score';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 30px;
  flex-grow: 1;
  max-width: 100%;
`;

const SubContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  gap: 30px;
  max-width: 100%;
`;

/** Renders the graphics container */
const ProfileBodyGraphics: React.FC<{ data: IUserData }> = ({ data }) => {
  return (
    <Container>
      <Activity activityData={data.activity} />
      <SubContainer>
        <AverageSessions averageSessionsData={data.averageSessions} />
        <Performance performanceData={data.performance} />
        <Score score={(data.infos.todayScore ?? data.infos.score) as number} />
      </SubContainer>
    </Container>
  );
};

export default ProfileBodyGraphics;
