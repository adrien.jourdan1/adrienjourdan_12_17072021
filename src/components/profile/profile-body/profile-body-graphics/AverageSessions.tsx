import React from 'react';
import styled from 'styled-components';
import { ResponsiveContainer, LineChart, XAxis, YAxis, Tooltip, Line } from 'recharts';
import colors from '../../../../utils/style/colors';
import { IUserDataAverageSessions, IUserDataAverageSessionsDay } from '../../../../utils/interfaces/api.interface';

const Container = styled.div`
  height: 265px;
  background-color: ${colors.secondary};
  border-radius: 5px;
  position: relative;
  width: 265px;
`;

const ChartTitle = styled.h3`
  margin: 0;
  font-size: 15px;
  font-weight: 400;
  position: absolute;
  top: 25px;
  left: 30px;
  opacity: 0.5;
  color: white;
  max-width: 150px;
  line-height: 24px;
`;

const CustomTooltip = styled.div`
  background-color: white;

  p {
    font-size: 12px;
    font-weight: 500;
    padding: 10px 7px;
  }
`;

/**
 * Renders the average sessions duration graphic
 * @param   {IUserDataAverageSessions}   averageSessionsData   contains user's average sessions duration data
 */
const AverageSessions: React.FC<{ averageSessionsData: IUserDataAverageSessions }> = ({ averageSessionsData }) => {
  enum WeekInitials {
    'L' = 1,
    'M',
    'J' = 4,
    'V',
    'S',
    'D',
  }

  // get the correct axis label from performance.kind
  const getAxisLabels = (data: IUserDataAverageSessionsDay) => {
    let { day } = data;

    if (day === 3) {
      day -= 1;
    }

    return WeekInitials[day];
  };

  const getCustomTooltip = (data: any) => {
    const { payload, active } = data;
    if (active && payload && payload.length) {
      return (
        <CustomTooltip>
          <p>{`${payload[0].payload.sessionLength} min`}</p>
        </CustomTooltip>
      );
    }

    return null;
  };

  return (
    <Container>
      <ResponsiveContainer width="100%" height={265}>
        <LineChart
          width={500}
          height={300}
          data={averageSessionsData.sessions}
          margin={{
            top: 80,
            right: 25,
            left: 25,
            bottom: 30,
          }}
        >
          <defs>
            <linearGradient id="linear">
              <stop offset="0%" stopColor="rgba(255,255,255,0.2)" />
              <stop offset="100%" stopColor="rgb(255,255,255)" />
            </linearGradient>
          </defs>
          <XAxis
            dataKey={getAxisLabels}
            axisLine={false}
            tickLine={false}
            dy={20}
            tick={{ fill: 'white', fontSize: 12, fontWeight: 500, opacity: 0.5 }}
          />
          <YAxis hide />
          <Tooltip content={getCustomTooltip} />
          <Line
            strokeWidth={3}
            type="monotone"
            dataKey="sessionLength"
            stroke="url(#linear)"
            dot={false}
            activeDot={{ r: 6, fill: 'white', strokeWidth: 12, stroke: 'rgba(255,255,255,0.15)' }}
          />
        </LineChart>
      </ResponsiveContainer>
      <ChartTitle>Durée moyenne des sessions</ChartTitle>
    </Container>
  );
};

export default AverageSessions;
