import React from 'react';
import styled from 'styled-components';
import { IUserData } from '../../../utils/interfaces/api.interface';
import ProfileBodyGraphics from './profile-body-graphics';
import ProfileBodyKeys from './profile-body-keys';

const Container = styled.div`
  margin-top: 75px;
  display: flex;
  flex-direction: row;
  gap: 30px;

  @media (max-width: 1350px) {
    flex-direction: column;
  }
`;

/** Renders the body of profile page */
const ProfileBody: React.FC<{ data: IUserData }> = ({ data }) => {
  return (
    <Container>
      <ProfileBodyGraphics data={data} />
      <ProfileBodyKeys data={data.infos} />
    </Container>
  );
};

export default ProfileBody;
