import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import colors from '../../utils/style/colors';

const Container = styled.header`
  display: flex;
  background-color: ${colors.primary};
  height: 91px;
  width: 100%;
`;

const Logo = styled.img`
  height: 57.7px;
`;

const Nav = styled.nav`
  margin: auto 90px auto 30px;
  width: 100%;
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: space-between;

  a {
    color: white;
    font-size: 24px;
    font-weight: 500;
    text-decoration: none;

    &:hover {
      color: ${colors.secondary};
    }
  }
`;

/** Renders the header of the website with main navigation and logo */
const Header: React.FC = () => {
  // Allow to switch between the two api returned id
  // To remove when the identification system will be ready !
  const [currentUserId, setCurrentUserId] = useState(12);
  const updateUserId = () => {
    setCurrentUserId(currentUserId === 12 ? 18 : 12);
  };

  return (
    <Container>
      <Nav>
        <Logo src="/assets/logo.png" className="logo" alt="kasa" />
        <Link to="/">Accueil</Link>
        <Link to={`/profile/${currentUserId}`} onClick={updateUserId}>
          Profil
        </Link>
        <Link to="/">Réglage</Link>
        <Link to="/">Communauté</Link>
      </Nav>
    </Container>
  );
};

export default Header;
