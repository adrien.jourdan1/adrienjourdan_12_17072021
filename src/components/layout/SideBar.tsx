import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import colors from '../../utils/style/colors';

const Container = styled.aside`
  display: flex;
  flex-direction: column;
  position: relative;
  justify-content: center;
  align-items: center;
  background-color: ${colors.primary};
  width: 115px;
  flex-shrink: 0;
`;

const Copyright = styled.p`
  font-weight: 500;
  font-size: 12px;
  color: white;
  margin: auto 0;
  white-space: nowrap;

  transform-origin: center;
  transform: rotate(-90deg);
`;

const Nav = styled.nav`
  display: flex;
  flex-flow: column nowrap;
  gap: 20px;
  margin: auto;

  a {
    display: flex;
    height: 64px;
    width: 64px;
    background-color: white;
    border-radius: 6px;

    img {
      height: 32px;
      margin: auto;
    }

    &:hover {
      color: ${colors.secondary};
    }
  }
`;

/** Renders the sidebar of the website with secondary navigation and copyrights */
const SideBar: React.FC = () => {
  return (
    <Container>
      <Nav>
        <Link to="/">
          <img src="/assets/yoga-icon.svg" alt="icon" />
        </Link>
        <Link to="/">
          <img src="/assets/swim-icon.svg" alt="icon" />
        </Link>
        <Link to="/">
          <img src="/assets/cycling-icon.svg" alt="icon" />
        </Link>
        <Link to="/">
          <img src="/assets/bodybuilding-icon.svg" alt="icon" />
        </Link>
      </Nav>
      <Copyright>Copyright, SportSee 2020</Copyright>
    </Container>
  );
};

export default SideBar;
