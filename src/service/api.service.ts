import {
  IUserDataActivity,
  IUserDataAverageSessions,
  IUserDataPerformance,
  IUserDataInfo,
} from '../utils/interfaces/api.interface';

export default async function fetchUserData(userId: number): Promise<any> {
  const url = `http://localhost:3000/`;
  const endpoints = {
    infos: `user/${userId}`,
    activity: `user/${userId}/activity`,
    averageSessions: `user/${userId}/average-sessions`,
    performance: `user/${userId}/performance`,
  };

  const responses = Object.entries(endpoints).map(async ([key, uri]) => {
    try {
      const response = await fetch(url + uri);
      const { data } = await response.json();
      return [
        key as string,
        data as IUserDataActivity | IUserDataAverageSessions | IUserDataInfo | IUserDataPerformance,
      ];
    } catch (error) {
      console.log(error);
    }
  });

  const promises: any[] = await Promise.all(responses);
  const data = Object.fromEntries(promises);

  return data;
}
