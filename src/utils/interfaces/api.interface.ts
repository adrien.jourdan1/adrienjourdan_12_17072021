export interface IUserData {
  infos: IUserDataInfo;
  activity: IUserDataActivity;
  averageSessions: IUserDataAverageSessions;
  performance: IUserDataPerformance;
}

export interface IUserDataInfo {
  id: number;
  userInfos: {
    firstName: string;
    lastName: string;
    age: number;
  };
  todayScore?: number;
  score?: number;
  keyData: {
    calorieCount: number;
    proteinCount: number;
    carbohydrateCount: number;
    lipidCount: number;
  };
}

export interface IUserDataActivity {
  id: number;
  sessions: {
    calories: number;
    day: string;
    kilogram: number;
  }[];
}

export interface IUserDataAverageSessions {
  id: number;
  sessions: IUserDataAverageSessionsDay[];
}

export interface IUserDataAverageSessionsDay {
  day: number;
  sessionLength: number;
}

export interface IUserDataPerformance {
  id: number;
  data: {
    kind: string;
    value: number;
  }[];
  kind: IUserDataPerformanceKind;
}

export interface IUserDataPerformanceKind {
  1: 'cardio';
  2: 'energy';
  3: 'endurance';
  4: 'strength';
  5: 'speed';
  6: 'intensity';
}
