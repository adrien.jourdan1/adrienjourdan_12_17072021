import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './components/layout/Header';
import SideBar from './components/layout/SideBar';
import Error from './pages/error';
import Profile from './pages/profile';
import './utils/style/global.css';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  max-width: 100vw;
  overflow: hidden;
`;

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Header />
      <Container>
        <SideBar />
        <Switch>
          <Route path="/profile/:profileId" component={Profile} />
          <Route component={Error} />
        </Switch>
      </Container>
    </Router>
  </React.StrictMode>,
  document.getElementById('root'),
);
