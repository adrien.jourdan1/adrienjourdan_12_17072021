import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import colors from '../../utils/style/colors';

const Container = styled.div`
  margin: auto;
  padding-bottom: 150px;

  nav {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 5px;

    a {
      text-decoration: none;
      font-weight: 500;
      color: grey;
      font-size: 25px;
      width: max-content;

      &:hover {
        color: ${colors.secondary};
      }
    }
  }
`;

const Heading = styled.h1`
  color: ${colors.secondary};
  margin: 0;
  font-weight: 600;
  font-size: 115px;
  text-align: center;
`;

const Subheading = styled.h2`
  color: ${colors.grey};
  margin: 15px 0 25px 0;
  font-weight: 500;
  font-size: 50px;
  text-align: center;
`;

const Error: React.FC = () => {
  return (
    <Container>
      <Heading>404</Heading>
      <Subheading>Oops, This Page Not Found !</Subheading>
      <nav>
        <Link to="/profile/12">Checkout the Karl&apos;s profile page</Link>
        <Link to="/profile/18">or the Cecilia&apos;s profile page</Link>
      </nav>
    </Container>
  );
};

export default Error;
