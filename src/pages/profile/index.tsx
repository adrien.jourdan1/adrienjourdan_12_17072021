import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import fetchUserData from '../../service/api.service';
import ProfileHeading from '../../components/profile/ProfileHeading';
import ProfileBody from '../../components/profile/profile-body';
import Error from '../error';
import { IUserData } from '../../utils/interfaces/api.interface';

const Container = styled.div`
  margin: 65px auto;
`;

const Profile: React.FC = () => {
  const [userData, setUserData] = useState<IUserData | undefined>(undefined);
  const [isDataLoading, setDataLoading] = useState(false);
  const { profileId } = useParams() as unknown as { profileId: number };

  const getUserDataFromAPI = async (userId: number) => {
    setDataLoading(true);
    try {
      const response = await fetchUserData(userId);
      setUserData(response);
    } catch (e) {
      console.log(e);
    } finally {
      setDataLoading(false);
    }
  };

  useEffect(() => {
    getUserDataFromAPI(profileId);
  }, [profileId]);

  if (isDataLoading || typeof userData === 'undefined') {
    // todo: replace by a loader
    return <div />;
  }

  if (!userData.infos) {
    return <Error />;
  }

  return (
    <Container>
      <ProfileHeading userFirstname={userData.infos.userInfos.firstName} />
      <ProfileBody data={userData} />
    </Container>
  );
};

export default Profile;
