[![OpenClassrooms](https://img.shields.io/badge/-OpenClassrooms-%237451eb)](https://openclassrooms.com/fr/)
[![React](https://img.shields.io/badge/-React-%2361dafb)](https://reactjs.org/)
[![Typescript](https://img.shields.io/badge/-Typescript-%233178c6)](https://www.typescriptlang.org/)
[![html](https://img.shields.io/badge/-Html-%23dd4b25)](https://developer.mozilla.org/en-US/docs/Web/HTML)
[![css](https://img.shields.io/badge/-CSS-%23264de4)](https://developer.mozilla.org/en-US/docs/Web/CSS)
[![Creative Commons](https://img.shields.io/badge/Creative%20Commons-BY--NC--ND-black)](https://creativecommons.org/)

![Logo Sportsee](https://user.oc-static.com/upload/2020/08/18/15977560509272_logo%20%285%29.png)

# Welcome to SportSee

### SportSee is the twelfth project of the Openclassrooms' Front-End developer path !

### Build an analytics dashboard with React

- Produce technical documentation for an application
- Ensuring the data quality of an application
- Develop advanced graphics using JavaScript libraries
- Interact with a web service

Let's have a look on the [Figma mockup](https://www.figma.com/file/BMomGVZqLZb811mDMShpLu/UI-design-Sportify-FR?node-id=0%3A1)

# Technologies

### language

- [Typescript](https://www.typescriptlang.org/) v4.0.3

### framework

- [React](https://reactjs.org/) v17.0.1

### dependencies

- [Styled Components](https://styled-components.com/) v5.3.0
- [Recharts](https://recharts.org/en-US/) v2.0.10
- [React Router](https://reactrouter.com/) v5.2.0

# Getting Started

### Prerequisites

- [NodeJS](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)

### Instructions

1. Get the [Backend API project](https://github.com/OpenClassrooms-Student-Center/P9-front-end-dashboard)
2. Clone this repository on your computer
3. Open a terminal window in the cloned project
4. Run the following commands:

```
# Install dependencies
yarn

# Start local dev server
yarn start
```

- _Don't forget to run the **Backend API**_
- Your server should now be running at http://locahost:3001
- For now, the project has only two users. Click on profile page navigation link will switch between the two them.

# Author

> Adrien JOURDAN -> check [my gitlab](https://gitlab.com/adrien.jourdan1) and [my linkedIn profile](https://www.linkedin.com/in/adrienjourdan/)

# Contributing

> [Hugo BOCQUET](https://www.linkedin.com/in/hugo-b-18804a197/) as OpenClassrooms mentor

> [Mohamed Nassim MEZIANI](https://www.linkedin.com/in/mezianimn/) as OpenClassrooms mentor

> [Openclassrooms](https://openclassrooms.com/fr/) as my learning platform
